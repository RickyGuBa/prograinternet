import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ProductoPage } from '../producto/producto';
import { ProductosService } from '../../services/productos.service';

/**
 * Generated class for the UsuarioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-usuario',
  templateUrl: 'usuario.html',
})
export class UsuarioPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public productosService: ProductosService) {
    this.vendedor = this.navParams.get('usuario');
    productosService.getProductosByVendedor().subscribe(
      (data) => {
        if(data){
          this.sinFiltro = data;
          this.productos = this.sinFiltro.filter( (d) => d.vendedor == this.vendedor.id )
        }
      }
    )
  }

  vendedor: any;
  productos = []
  sinFiltro: any;

  editar(id){
    this.navCtrl.push(ProductoPage, { usuario: this.vendedor.id, id, editar: true, arr: this.sinFiltro });
  }

  nuevo(){
    let id = Date.now()
    this.navCtrl.push(ProductoPage, { usuario: this.vendedor.id, id, editar: false, arr: this.sinFiltro });
  }

  borrar(id){
    let arr = this.sinFiltro.filter(p => p.id != id)
    this.productosService.setProducto(arr);
  }

}
