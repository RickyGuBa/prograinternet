import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { VendedorPage } from '../vendedor/vendedor';
import { VendedoresService } from '../../services/vendedores.service';
import { ProductosService } from '../../services/productos.service';

/**
 * Generated class for the AdminPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-admin',
  templateUrl: 'admin.html',
})
export class AdminPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public vendedoresService: VendedoresService, public productosService: ProductosService) {
    vendedoresService.getVendedores().subscribe(
      (data) => {
        this.vendedores = data
      }
    )
  }

  vendedores: any;

  editar(id){
    this.navCtrl.push(VendedorPage, { id, editar: true, arr: this.vendedores });
  }

  nuevo(){
    let id = Date.now()
    this.navCtrl.push(VendedorPage, { id, editar: false, arr: this.vendedores });
  }

  borrar(id){
    let arr = this.vendedores.filter(p => p.id != id)
    this.productosService.deleteProductos(id);
    this.vendedoresService.setVendedor(arr);
  }

}
