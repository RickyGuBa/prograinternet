import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { VendedoresService } from '../../services/vendedores.service';
import { Camera, CameraOptions } from '@ionic-native/camera';

/**
 * Generated class for the VendedorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-vendedor',
  templateUrl: 'vendedor.html',
})
export class VendedorPage {
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public vendedoresService: VendedoresService, private camera:Camera) {
    this.vendedor.id = this.navParams.get('id');
    this.arr = this.navParams.get('arr');
    this.esEdicion = this.navParams.get('editar');
    if(this.esEdicion){
      this.vendedor = this.arr.find( v => v.id === this.vendedor.id)
    }
  }
  
  miFoto: any;
  arr = []
  esEdicion = false;
  vendedor = {
    id: 0,
    nombre: "",
    pass: "",
    ubicacion: "", 
    coords: [null, null],
    img: ""
  }

  listo() {
    this.vendedor.coords = [Number(this.vendedor.coords[0]), Number(this.vendedor.coords[1])]
    if(this.esEdicion){
      this.arr = this.arr.reduce((acc, n) => 
        {
          if(n.id == this.vendedor.id){
            return [...acc, this.vendedor]
          }
          else{
            return [...acc, n]
          }
        }, []
      )
    }
    else{
      if(!this.arr){
        this.arr = []
      }
      this.arr.push(this.vendedor)
    }
    this.vendedoresService.setVendedor(this.arr)
    this.navCtrl.pop();
  }

  cancelar() {
    this.navCtrl.pop();
  }

  tomarFoto(){
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      this.miFoto = 'data:image/jpeg;base64,' + imageData;
      this.vendedor.img = this.miFoto;
    }, (err) => {
      // Handle error
    });
  }

}
