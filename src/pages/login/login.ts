import { Component } from '@angular/core';
import { NavController, NavParams, AlertController  } from 'ionic-angular';
import { UsuarioPage } from './../usuario/usuario';
import { AdminPage } from '../admin/admin';
import { VendedoresService } from '../../services/vendedores.service';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public vendedoresService: VendedoresService) {
    vendedoresService.getVendedores().subscribe(
      (data) => {
        if(data){
          this.vendedores = data
        }
      }
    )
  }

  estaCompleto = false;
  tieneUsuario = false;
  tieneContra = false;
  data = {}
  usuario = []
  vendedores: any;

  credencial = {}

  validar(d){
    this.tieneUsuario = !!d.usuario;
    this.tieneContra = !!d.contra;
    this.estaCompleto = !!(d.usuario && d.contra)
  }

  validarDatos(d){
    let val = false;
    this.vendedores.forEach( v => {
      if(v.nombre === d.usuario){
        this.credencial = v
        val = true
      }
    })
    return val
  }

  datosIncorrectos(mensaje) {
    let alert = this.alertCtrl.create({
      title: 'Datos incorrectos',
      subTitle: mensaje,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  ingresar(d){
    if(d.usuario === 'admin' && d.contra === '0'){
      this.reset()
      this.navCtrl.push(AdminPage);
    }
    else{
      if(this.validarDatos(d)){
        this.reset()
        this.navCtrl.push(UsuarioPage, {usuario: this.credencial});
      }
      else{
        this.datosIncorrectos("Verifica usuario y contraseña.")
      }
    }
  }

  reset() {
    this.data = {
      usuario: '',
      contra: ''
    }
    this.estaCompleto = false;
    this.tieneUsuario = false;
    this.tieneContra = false;
  }


}
