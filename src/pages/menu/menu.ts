import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ListaPage } from './../lista/lista';

import { VendedoresService } from '../../services/vendedores.service';

@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html'
})
export class MenuPage {

  constructor(public navCtrl: NavController, public vendedoresService: VendedoresService) {
    vendedoresService.getVendedores().subscribe(
      (data) => {
        this.vendedores = data
      }
    )
  }

  page = ListaPage;

  vendedores: any;

}
