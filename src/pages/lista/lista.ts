import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ProductosService } from '../../services/productos.service';

/**
 * Generated class for the ListaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-lista',
  templateUrl: 'lista.html',
})
export class ListaPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public productosService: ProductosService) {
    this.idVendedor = this.navParams.get('id');
    this.nombreVendedor = this.navParams.get('nombre');
    productosService.getProductosByVendedor().subscribe(
      (data) => {
        if(data){
          let d: any;
          d = data;
          this.productos = d.filter( (d) => d.vendedor == this.idVendedor )
        }
      }
    )
  }

  nombreVendedor = ''
  idVendedor = 0
  productos = []

}
