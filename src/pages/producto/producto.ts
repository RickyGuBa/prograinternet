import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ProductosService } from '../../services/productos.service';
import { Camera, CameraOptions } from '@ionic-native/camera';

/**
 * Generated class for the ProductoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-producto',
  templateUrl: 'producto.html',
})
export class ProductoPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public productosService: ProductosService, private camera:Camera) {
    this.producto.id = this.navParams.get('id');
    this.arr = this.navParams.get('arr');
    this.producto.vendedor = this.navParams.get('usuario');
    this.esEdicion = this.navParams.get('editar');
    if(this.esEdicion){
      this.producto = this.arr.find( v => v.id === this.producto.id)
    }
  }

  miFoto: any;
  esEdicion = false;
  arr = []
  producto = {
    id: 0,
    nombre: '',
    descripcion: '',
    precio: null,
    vendedor: 0,
    img: ''
  }

  listo() {
    this.producto.precio = Number(this.producto.precio)
    if(this.esEdicion){
      this.arr = this.arr.reduce((acc, n) => 
        {
          if(n.id == this.producto.id){
            return [...acc, this.producto]
          }
          else{
            return [...acc, n]
          }
        }, []
      )
    }
    else{
      if(!this.arr){
        this.arr = []
      }
      this.arr.push(this.producto)
    }
    this.productosService.setProducto(this.arr)
    this.navCtrl.pop();
  }

  tomarFoto(){
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      this.miFoto = 'data:image/jpeg;base64,' + imageData;
      this.producto.img = this.miFoto;
    }, (err) => {
      // Handle error
    });
  }

  cancelar() {
    this.navCtrl.pop();
  }

}
