import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';

import { GoogleMaps, GoogleMap, GoogleMapOptions } from "@ionic-native/google-maps";
import { VendedoresService } from '../../services/vendedores.service';

/**
 * Generated class for the DetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {

  mapReady: boolean = false;
  map: GoogleMap;
  vendedores = [];

  constructor(public platform: Platform, public navCtrl: NavController, public navParams: NavParams, public vendedoresService: VendedoresService) {
    vendedoresService.getVendedores().subscribe(
      (data) => {
        if(data){
          let d:any;
          d = data;
          this.vendedores = d;
          this.vendedores.forEach((v) => {
            this.map.addMarkerSync({
              title: v.nombre,
              icon: 'blue',
              animation: 'DROP',
              position: {
                lat: v.coords[0],
                lng: v.coords[1]
              }
            })
          });
        }
      }
    )
  }

  
  ngAfterViewInit() {
    this.platform.ready().then(() => {
      this.loadMap();
    });
  }

  loadMap(){
    let mapOptions: GoogleMapOptions = {
      camera: {
         target: {
           lat: 20.657231046019177,
           lng: -103.3257385773752
         },
         zoom: 16,
         tilt: 30
       }
    };

    this.map = GoogleMaps.create('map', mapOptions);
    
  }

}
