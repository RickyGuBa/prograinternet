import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Camera } from '@ionic-native/camera';

import { MyApp } from './app.component';
import { MenuPage } from '../pages/menu/menu';
import { MapPage } from '../pages/map/map';
import { LoginPage } from '../pages/login/login';
import { ListaPage } from '../pages/lista/lista';
import { UsuarioPage } from '../pages/usuario/usuario';
import { ProductoPage } from '../pages/producto/producto';
import { AdminPage } from '../pages/admin/admin';
import { VendedorPage } from './../pages/vendedor/vendedor';

import { ProductosService } from '../services/productos.service';
import { VendedoresService } from '../services/vendedores.service';

import { AngularFireModule } from "angularfire2";
import { AngularFireDatabase, AngularFireDatabaseModule } from 'angularfire2/database';

import { GoogleMaps } from "@ionic-native/google-maps";

const config = {
  apiKey: "AIzaSyARmerBOjCbUbisFLmi9pJehYRSJ-BVFbQ",
  authDomain: "prograinternet-1689b.firebaseapp.com",
  databaseURL: "https://prograinternet-1689b.firebaseio.com",
  projectId: "prograinternet-1689b",
  storageBucket: "prograinternet-1689b.appspot.com",
  messagingSenderId: "839668212764"
};

@NgModule({
  declarations: [
    MyApp,
    MenuPage,
    MapPage,
    LoginPage,
    ListaPage,
    UsuarioPage,
    ProductoPage,
    AdminPage,
    VendedorPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: ''
    }),
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(config),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MenuPage,
    MapPage,
    LoginPage,
    ListaPage,
    UsuarioPage,
    ProductoPage,
    AdminPage,
    VendedorPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ProductosService,
    VendedoresService,
    AngularFireDatabase,
    GoogleMaps,
    Camera
  ]
})
export class AppModule {}
