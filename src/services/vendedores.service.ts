import { Injectable } from '@angular/core';
import { AngularFireDatabase } from "angularfire2/database";

@Injectable()
export class VendedoresService{
    
    constructor(public db: AngularFireDatabase){
    }
    
    public getVendedores(){
        return this.db.object('vendedores/').valueChanges();
    }

    public setVendedor(data){
        this.db.database.ref('vendedores/').set(data);
    }
    
}