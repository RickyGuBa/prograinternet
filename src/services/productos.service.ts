import { Injectable } from '@angular/core';
import { AngularFireDatabase } from "angularfire2/database";

@Injectable()
export class ProductosService{
    
    constructor(public db: AngularFireDatabase){
    }
    
    public getProductosByVendedor(){
        return this.db.object('/productos').valueChanges();
    }

    public setProducto(data){
        this.db.database.ref('/productos').set(data);
    }

    public deleteProductos(id){
        let arr: any;
        this.getProductosByVendedor().subscribe(
            (data) => {
                if(data){
                  arr = data;
                  arr = arr.filter((d) => d.vendedor != id)
                  this.setProducto(arr);
                }
              }
        )
    }
    
}